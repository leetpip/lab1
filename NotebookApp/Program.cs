﻿using System;

public class Notebook
{
    public static void Main(string[] args)
    { 
        NoteActivity noteActivity = new NoteActivity();

        bool check = false;

        Console.WriteLine("Для начала, давайте создадим первую заметку!\n");
        Note note = new Note();
        note.CreateNewNote();
        noteActivity.AddNote(note);

        string count;

        while (check != true)
        {
            Console.WriteLine("Может вы хотите сделать что-нибудь помимо создания заметки? Введите одну из цифр:\n" +
                "1. Создать ещё одну\n" +
                "2. Редактировать одну из заметок\n" +
                "3. Удалить одну из заметок\n" +
                "4. Посмотреть одну из заметок\n" +
                "5. Посмотреть все возможные заметки (только важную информацию)\n" +
                "0. Закончить работу\n");

            count = Console.ReadLine();
            if (int.TryParse(count, out int counter)) counter = int.Parse(count);

            switch (counter)
            {
                case 0:
                    check = true;
                    Console.WriteLine("Работа закончена!\n");
                    break;
                case 1:
                    note = new Note();
                    note.CreateNewNote();
                    noteActivity.AddNote(note);
                    break;
                case 2:
                    Console.WriteLine("Какую заметку вы хотите редактировать?\n");
                    count = Console.ReadLine();
                    if (int.TryParse(count, out int b))
                    {
                        b = int.Parse(count);
                        noteActivity.EditNote(b);
                    }
                    else Console.WriteLine("Лучше введите число!\n");
                    break;
                case 3:
                    Console.WriteLine("Какую заметку вы хотите удалить?\n");
                    count = Console.ReadLine();
                    if (int.TryParse(count, out int c))
                    {
                        c = int.Parse(count);
                        noteActivity.DeleteNote(c);
                    }
                    else Console.WriteLine("Лучше введите число!\n");
                    break;
                case 4:
                    Console.WriteLine("Какую заметку вы хотите прочитать?\n");
                    count = Console.ReadLine();
                    if (int.TryParse(count, out int d))
                    {
                        d = int.Parse(count);
                        noteActivity.ReadNote(d);
                    }
                    else Console.WriteLine("Лучше введите число!\n");
                    break;
                case 5:
                    noteActivity.ShowAllNotes();
                    break;
            }

        }
    }

    //public static void Parser(int count, int a)
    //{
    //    a = 0;
    //    if (int.TryParse(count, out int a)) d = int.Parse(count);
    //}

}

public class NoteActivity
{
    private static List<Note> notes = new List<Note>();

    public void EditNote(int num)
    {
        if (num >= notes.Count || num < 0 || notes[num] == null) Console.WriteLine("Заметка удалена или вы ввели число за пределами ваших заметок!\n");
        else notes[num].EditNote(num);
    }

    public void AddNote(Note note)
    {
        notes.Add(note);
    }

    public void DeleteNote(int num)
    {
        if (num >= notes.Count || num < 0 || notes[num] == null) Console.WriteLine("Заметка удалена или вы ввели число за пределами ваших заметок!\n");
        else notes[num] = null;
    }

    public void ReadNote(int num)
    {
        if (num >= notes.Count || num < 0 || notes[num] == null) Console.WriteLine("Заметка удалена или вы ввели число за пределами ваших заметок!\n");
        else
        {
            Console.WriteLine($"Имя {num}-го: {notes[num].GetFirstName()}\n" +
                $"Фамилия {num}-го: {notes[num].GetLastName()}\n" +
                $"Отчество {num}-го: {notes[num].GetMiddleName()}\n" +
                $"Номер телефона {num}-го: {notes[num].GetPhoneNumber()}\n" +
                $"Страна {num}-го: {notes[num].GetCountry()}\n" +
                $"Дата рождения {num}-го: {notes[num].GetBirthDate()}\n" +
                $"Организация {num}-го: {notes[num].GetOrganization()}\n" +
                $"Должность {num}-го: {notes[num].GetWorkPost()}\n" +
                $"Прочие заметки {num}-го: {notes[num].GetOtherNote()}\n");
        }
    }

    public void ShowAllNotes()
    {
        for (int i = 0; i < notes.Count; i++)
        {
            if (notes[i] == null) Console.WriteLine("Заметка удалена!\n");
            else
            {
                Console.WriteLine($"Имя {i}-го: {notes[i].GetFirstName()}\n" +
                    $"Фамилия {i}-го: {notes[i].GetMiddleName()}\n" +
                    $"Номер телефона {i}-го: {notes[i].GetPhoneNumber()}\n");
            }
        }
    }
} 


public class Note
{
    public string firstName; // Имя
    private string lastName; // Фамилия
    private string middleName; // Отчество (необязательно)
    private int phoneNumber; // Только цифры
    private string country;
    private string birthDate; // Формат - 15.12.2000 (необязательно)
    private string organization; // Необязательно
    private string workPost; // Должность (необязательно)
    private string otherNote; // Прочие заметки (необязательно)

    public string GetFirstName() { return firstName; }
    public string GetLastName() { return lastName; }
    public int GetPhoneNumber() { return phoneNumber; }
    public string GetCountry() { return country; }

    public string GetMiddleName() { return middleName; }
    public string GetBirthDate() { return birthDate; }
    public string GetOrganization() { return organization; }
    public string GetWorkPost() { return workPost; }
    public string GetOtherNote() { return otherNote; }

    public void EditNote(int num)
    {
        Console.WriteLine("Что вы хотите изменить?\n" +
            "1. Имя\n2. Фамилия\n3. Отчество\n4. Номер телефона\n5. Страна\n" +
            "6. Дата рождения\n7. Организация\n8. Должность.\n9. Прочие заметки.\n0. Закончить.\n");
            bool check = false;
            while (check != true)
            {
                var myChoice = Console.ReadLine();
                if ("0 1 2 3 4 5 6 7 8 9".Contains(myChoice) && int.TryParse(myChoice, out int a))
                {
                    int choice = int.Parse(myChoice);
                    if (choice == 0)
                    {
                        check = true;
                        Console.WriteLine("Заметка редактирована!");
                    }
                    else
                    {
                        string myData = Console.ReadLine();
                        switch (choice)
                        {
                            case 1:
                                Console.WriteLine("Вы ввели новое имя!\n");
                                firstName = myData;
                                break;
                            case 2:
                                Console.WriteLine("Вы ввели новую фамилию!\n");
                                lastName = myData;
                                break;
                            case 3:
                                Console.WriteLine("Вы ввели новое отчество!\n");
                                middleName = myData;
                                break;
                            case 4:
                                if (int.TryParse(myData, out choice))
                                {
                                    Console.WriteLine("Вы ввели новый номер телефона!\n");
                                    phoneNumber = int.Parse(myData);
                                }
                                else Console.WriteLine("Ошибка парсинга! При вводе номера телефона вы ввели не число.\n" +
                                    "Если хотите попробовать снова, введите 4 и корректный номер.\n");
                                break;
                            case 5:
                                Console.WriteLine("Вы ввели новую страну!\n");
                                country = myData;
                                break;
                            case 6:
                                Console.WriteLine("Вы ввели новую дату рождения!\n");
                                birthDate = myData;
                                break;
                            case 7:
                                Console.WriteLine("Вы ввели новую организацию!\n");
                                organization = myData;
                                break;
                            case 8:
                                Console.WriteLine("Вы ввели новую рабочую должность!\n");
                                workPost = myData;
                                break;
                            case 9:
                                Console.WriteLine("Вы ввели новую дополнительную заметку!\n");
                                otherNote = myData;
                                break;
                        }
                    }
                }
                else Console.WriteLine("Лучше введите число из списка!\n");
            }
    }

    //public override string ToString()
    //{
    //    return $"Имя: {firstName}\n" +
    //            $"Фамилия: {lastName}\n" +
    //            $"Отчество: {middleName}\n" +
    //            $"Номер телефона: {phoneNumber}" +
    //            $"Страна: {country}" +
    //            $"Дата рождения: {birthDate}" +
    //            $"Организация: {organization}" +
    //            $"Должность: {workPost}" +
    //            $"Прочие заметки: {otherNote})";
    //}

    public void CreateNewNote()
    {
        Console.WriteLine("Привет! Я твоя записная книжка! Давай создадим твою первую заметку!" +
            "\nЧто ты хочешь ввести первым делом? Выбери одну из подходящих цифр и введи нужные данные!" +
            "\n\nУчтите, записывая новые данные поверх старых (например, имя) до окончания создания заметки," +
            "\nвы их перезапишите, а не создадите новые!\n");
        Console.WriteLine("1. Имя\n2. Фамилия\n3. Отчество\n4. Номер телефона\n5. Страна\n" +
            "6. Дата рождения\n7. Организация\n8. Должность.\n9. Прочие заметки.\n0. Закончить.\n");
        bool check = false;
        while (check != true)
        {
            var myChoice = Console.ReadLine();
            if ("0 1 2 3 4 5 6 7 8 9".Contains(myChoice) && int.TryParse(myChoice, out int a))
            {
                int choice = int.Parse(myChoice);
                if (choice == 0)
                {
                    if (firstName == "" || lastName == "" || phoneNumber == 0 || country == "")
                        Console.WriteLine("\nВы ввели не все обязательные данные! В заметке необходимо указать как минимум:\n" +
                        "имя, фамилию, номер телефона и страну\n");
                    else
                    {
                        check = true;
                        Console.WriteLine("\nЗаметка заполнена!\n");
                    } 
                }
                else
                {
                    string myData = Console.ReadLine();
                    switch (choice)
                    {
                        case 1:
                            Console.WriteLine("Вы ввели имя!\n");
                            firstName = myData;
                            break;
                        case 2:
                            Console.WriteLine("Вы ввели фамилию!\n");
                            lastName = myData;
                            break;
                        case 3:
                            Console.WriteLine("Вы ввели отчество!\n");
                            middleName = myData;
                            break;
                        case 4:
                            if (int.TryParse(myData, out choice))
                            {
                                Console.WriteLine("Вы ввели номер телефона!\n");
                                phoneNumber = int.Parse(myData);
                            }
                            else Console.WriteLine("Ошибка парсинга! При вводе номера телефона вы ввели не число.\n" +
                                "Если хотите попробовать снова, введите 4 и корректный номер.\n");
                            break;
                        case 5:
                            Console.WriteLine("Вы ввели страну!\n");
                            country = myData;
                            break;
                        case 6:
                            Console.WriteLine("Вы ввели дату рождения!\n");
                            birthDate = myData;
                            break;
                        case 7:
                            Console.WriteLine("Вы ввели организацию!\n");
                            organization = myData;
                            break;
                        case 8:
                            Console.WriteLine("Вы ввели рабочую должность!\n");
                            workPost = myData;
                            break;
                        case 9:
                            Console.WriteLine("Вы ввели дополнительную заметку!\n");
                            otherNote = myData;
                            break;
                    }
                }
            }
            else Console.WriteLine("Лучше введите число из списка!\n");
        }
    }
}